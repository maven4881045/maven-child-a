package org.example.a;

/**
 * Hello world!
 */
public class AppA {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        printA();
    }

    public static void printA() {
        System.out.println("App A");
        org.example.b.AppB.printB();
    }
}
